///draw_hp_bar(x, y)
var xx = argument0;
var yy = argument1;
draw_hp = smooth_approach(draw_hp, hp, .1);
if (point_distance(draw_hp, 0, hp, 0) < 1) draw_hp = hp;
draw_sprite(s_hp_bar, (draw_hp/max_hp)*15, xx, yy);

