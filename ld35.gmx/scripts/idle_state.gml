///idle_state()
var coin = instance_position(x, y-CELL_HEIGHT/2, o_coin)
if (coin) {
    with (coin) instance_destroy();
    o_creature_stats.coins+=3;
}

var enemy = instance_position(x, y-CELL_WIDTH/2, o_enemy_encounter);
if (enemy) {
    with (enemy) instance_destroy()
    room_persistent = true;
    room_goto(r_battle);
}

if (position_meeting(x, y-CELL_HEIGHT/2, o_enemy_boss_encounter)) {
    with (o_enemy_boss_encounter) instance_destroy()
    room_persistent = true;
    room_goto(r_boss);
    exit;
}

if (position_meeting(x, y-CELL_HEIGHT/2, o_cave_exit)) {
    if (o_game.cave_level != 3) {
        room_goto(r_choice);
    } else {
        room_goto(r_home);
        o_game.cave_level = 1;
    }
}

if (mouse_check_button(mb_left)) {
    var xdis = mouse_x-x; 
    var ydis = mouse_y-y;
    
    if (abs(xdis) > abs(ydis)) {
        last_x = x;
        if (sign(xdis)) {
            if (!grid_position_meeting(x+CELL_WIDTH, y-CELL_HEIGHT/2, DIRT)) {
                state = move_right;
            }
        } else {
            if (!grid_position_meeting(x-CELL_WIDTH, y-CELL_HEIGHT/2, DIRT)) {
                state = move_left;
            }
        }
    } else {
        last_y = y;
        if (sign(ydis)) {
            if (!grid_position_meeting(x, y-(CELL_HEIGHT/2)+CELL_HEIGHT, DIRT)) {
                state = move_down;
            }
        } else {
            if (!grid_position_meeting(x, y-(CELL_HEIGHT/2)-CELL_HEIGHT, DIRT)) {
                state = move_up;
            }
        }
    }
}
