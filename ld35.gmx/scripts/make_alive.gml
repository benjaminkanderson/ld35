///make_alive(offset)
var offset = argument0;
image_xscale = smooth_approach(image_xscale, 1+(sin(o_game.sin_input+offset)*.05), .2);
image_yscale = smooth_approach(image_yscale, 1+(sin(o_game.sin_input+pi+offset)*.05), .2);
