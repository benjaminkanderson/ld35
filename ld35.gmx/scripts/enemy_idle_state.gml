///enemy_idle_state()
if (alarm[0] <= 0) {
    random_range(room_speed*3, room_speed*5);
    attacky = choose(48, room_height-48);
    if (attacky == 48) {
        image_xscale = 1.3;
        image_yscale = 1.3;
    } else {
        image_xscale = .8;
        image_yscale = .8;
    }
    state = enemy_attack_state;
}
