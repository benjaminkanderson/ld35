///create_particles(x, y, number, sprite)
var xx = argument0;
var yy = argument1;
var number = argument2;
var sprite = argument3;

repeat (number) {
    var part = instance_create(xx, yy, o_particle);
    part.sprite_index = sprite;
}
