///grid_position_meeting(x, y, cell_type)
var xx = argument0;
var yy = argument1;
var cell_type = argument2;

var gx = xx div CELL_WIDTH;
var gy = yy div CELL_HEIGHT;

return o_cave.grid[# gx, gy] == cell_type;
