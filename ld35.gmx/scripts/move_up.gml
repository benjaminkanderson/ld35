///move_up
y = smooth_approach(y, last_y-CELL_HEIGHT, move_speed);
if (point_distance(y, 0, last_y-CELL_HEIGHT, 0) < .5) {
    y = last_y-CELL_HEIGHT;
    state = idle_state;   
}
