///enemy_attack_state()
var dir = point_direction(xstart, ystart, 32, attacky);
var dis = point_distance(xstart, ystart, 32, attacky);
var xspeed = abs(lengthdir_x(dis/30, dir));
var yspeed = abs(lengthdir_y(dis/30, dir));
if (x != 32 || y != attacky) {
    x = approach(x, 32, xspeed);
    y = approach(y, attacky, yspeed);
    if (place_meeting(x, y, o_battle_creature) && !attacked) {
        o_creature_stats.hp -= attack;
        attacked = true;
        instance_create(0, 0, o_red_flash);
    }
} else {
    state = enemy_return_state;
}
