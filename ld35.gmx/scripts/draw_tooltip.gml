///draw_tooltip(x, y, string)
var xx = argument0;
var yy = argument1;
var str = argument2;
var str_width = string_width(str)*2;
var str_height = string_height(str)*2;
draw_set_alpha(.5);
draw_set_halign(fa_middle);
draw_rectangle_colour(xx-(str_width/2)-8, yy, xx+(str_width/2)+8, yy+str_height+8, c_black, c_black, c_black, c_black, false);
draw_set_alpha(1);
draw_scaled_text(xx, yy+4, str);
