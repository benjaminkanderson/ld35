///enemy_return_state()
if (x != xstart || y != ystart) {
    x = approach(x, xstart, 8);
    y = approach(y, ystart, 2);
} else {
    attacked = false;
    state = enemy_idle_state;
    alarm[0] = room_speed*irandom_range(1, 3);
}
