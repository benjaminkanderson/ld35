///battle_attack_state()

if (x != room_width-80) {
    x = approach(x, room_width-80, 32);
    if (!attacked && place_meeting(x, y, o_enemy_creature)) {
        attacked = true;
        o_enemy_creature.hp -= o_creature_stats.attack;
        create_particles(o_enemy_creature.x, o_enemy_creature.y, 10, s_bullet);
    }
} else {
    state = battle_return_state;
}
