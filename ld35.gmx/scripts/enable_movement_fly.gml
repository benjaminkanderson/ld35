///enable_movement_fly(height, input, release_input)
/*
    Call this script to enable platform jumping
    on a movement entity.
*/

var height = argument[0]; // The jump height (Should be positive)
var input = argument[1]; // The input for jumping
var release_input = argument[2]; // The input for jump height control (release)

// Check for fly
if (input) {
    sprite_index = s_player_fly;
    image_index = 0;
    image_speed = .25;
    vsp[0] = -height;
} else {
    if (release_input && vsp[0] <= -height/3) {
        vsp[0] = -height/3;
    }
}

if (animation_end()) {
    image_speed = 0;
}
