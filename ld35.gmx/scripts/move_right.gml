///move_right
x = smooth_approach(x, last_x+CELL_WIDTH, move_speed);
if (point_distance(x, 0, last_x+CELL_WIDTH, 0) < .5) {
    x = last_x+CELL_WIDTH;
    state = idle_state;   
}
